package com.darien.myapplication.Models;

public class Product {
    private String title, imageUrl, location;
    private double price;

    public String getTitle() {return title;}
    public String getImageUrl() {return imageUrl;}
    public String getLocation() {return location;}
    public double getPrice() {return price;}

    public Product(String title, String imageUrl, String location, double price) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.location = location;
        this.price = price;
    }
}
