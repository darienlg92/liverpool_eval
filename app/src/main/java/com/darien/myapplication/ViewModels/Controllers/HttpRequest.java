package com.darien.myapplication.ViewModels.Controllers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.darien.myapplication.Models.MySingleton;
import com.darien.myapplication.Models.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HttpRequest {
    private LiverpoolDataReceiver liverpoolDataReceiver;
    private static final int ITEMS_PER_PAGE = 10;

    public void setLiverpoolDataReceiver(LiverpoolDataReceiver liverpoolDataReceiver) {
        this.liverpoolDataReceiver = liverpoolDataReceiver;
    }

    public HttpRequest() {
    }

    public void searchItem(String query, int page, Context context){
        String url = "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/plp?force-plp=true&search-string="
                .concat(query).concat("&page-number=").concat(String.valueOf(page)).concat("&number-of-items-per-page=")
                .concat(String.valueOf(HttpRequest.ITEMS_PER_PAGE));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (liverpoolDataReceiver != null) {
                            ArrayList<Product> products = getProductsList(response);
                            if (products != null){
                                liverpoolDataReceiver.onReceive(products);
                            }else {
                                liverpoolDataReceiver.onErrorReceiving();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (liverpoolDataReceiver != null){
                    liverpoolDataReceiver.onNetworkError();
                }
            }
        });
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    private ArrayList<Product> getProductsList(JSONObject response){
        ArrayList<Product> responseList = new ArrayList<>();
        try{
            JSONObject plpResults = response.getJSONObject("plpResults");
            JSONArray records = plpResults.getJSONArray("records");
            int recordsLength = records.length();
            for (int i = 0; i < recordsLength; i++){
                responseList.add(new Product(records.getJSONObject(i).getString("productDisplayName"),
                        records.getJSONObject(i).getString("smImage"), "",
                        records.getJSONObject(i).getDouble("listPrice")));
            }

            return  responseList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public interface LiverpoolDataReceiver{
        void onReceive(ArrayList<Product> productsList);
        void onErrorReceiving();
        void onNetworkError();
    }
}
