package com.darien.myapplication.ViewModels.Controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darien.myapplication.Models.Product;
import com.darien.myapplication.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private ArrayList<Product> products;
    private HashMap<String, Bitmap> loadedImages;
    private Context context;

    public HashMap<String, Bitmap> getLoadedImages() {return loadedImages;}

    public ArrayList<Product> getProducts() {return products;}

    public RecyclerViewAdapter(ArrayList<Product> products, Context context) {
        this.products = products;
        this.context = context;
        loadedImages = new HashMap<>();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.product_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(products.get(position).getTitle());
        holder.address.setText(products.get(position).getLocation());
        holder.price.setText(String.valueOf(products.get(position).getPrice()).concat(" MXN"));
        if (loadedImages.get(products.get(position).getImageUrl()) != null){
            holder.putImage(loadedImages.get(products.get(position).getImageUrl()));
        }else {
            holder.readImageFromUrl(products.get(position).getImageUrl());
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView title, address, price;
        ImageView productImage;
        ProgressBar pbLoadingImage;
        private MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.product_name);
            address = itemView.findViewById(R.id.product_address);
            price = itemView.findViewById(R.id.product_price);
            productImage = itemView.findViewById(R.id.product_image);
            pbLoadingImage = itemView.findViewById(R.id.pb_loading_image);
        }

        private void readImageFromUrl(String urlImage){
            showLoading();
            Thread t = new Thread(()->{
                try{
                    InputStream stream = new java.net.URL(urlImage).openStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(stream);
                    if (bitmap != null) {
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(() -> {
                            putImage(bitmap);
                            loadedImages.put(urlImage, bitmap);
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
            t.start();
        }
        private void putImage(Bitmap bitmap){
            pbLoadingImage.clearAnimation();
            productImage.setImageBitmap(bitmap);
            pbLoadingImage.setVisibility(View.GONE);
            productImage.setVisibility(View.VISIBLE);
        }

        private void showLoading(){
            pbLoadingImage.setVisibility(View.VISIBLE);
            productImage.setVisibility(View.INVISIBLE);
            pbLoadingImage.animate();
        }
    }

}
