package com.darien.myapplication.ViewModels;

import android.view.View;
import android.widget.ArrayAdapter;


import com.darien.myapplication.Models.Product;
import com.darien.myapplication.ViewModels.Controllers.HttpRequest;
import com.darien.myapplication.ViewModels.Controllers.MainActivityViewHandlers;
import com.darien.myapplication.ViewModels.Controllers.PrevSearchesDBHandler;
import com.darien.myapplication.ViewModels.Controllers.RecyclerViewAdapter;
import com.darien.myapplication.Views.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivityViewModel {
    private MainActivity mainActivity;
    private PrevSearchesDBHandler dbHandler;
    private String lastQuery;
    private ArrayAdapter<String> suggestionsAddapter;
    private int lastPageChecked;
    private boolean scrolled, requesting;
    private RecyclerViewAdapter rvAdapter;
    private MainActivityViewHandlers mainActivityViewHandlers;
    private HttpRequest httpRequestController;

    public void setRvAdapter(RecyclerViewAdapter rvAdapter) {this.rvAdapter = rvAdapter;}
    public RecyclerViewAdapter getRvAdapter() {
        return rvAdapter;
    }
    public MainActivity getMainActivity() {
        return mainActivity;
    }
    public ArrayAdapter<String> getSuggestionsAddapter() {
        return suggestionsAddapter;
    }
    public boolean isScrolled() {
        return scrolled;
    }

    public boolean isRequesting() {
        return requesting;
    }

    public void setRequesting(boolean requesting) {
        this.requesting = requesting;
    }

    public MainActivityViewModel(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        lastPageChecked = 1;
        scrolled = false;
        requesting = false;
        lastQuery = "";
        dbHandler = new PrevSearchesDBHandler(mainActivity.getApplicationContext());
        httpRequestController = new HttpRequest();
        suggestionsAddapter = new ArrayAdapter<>(mainActivity, android.R.layout.simple_list_item_1, dbHandler.getQueries());
        httpRequestController.setLiverpoolDataReceiver(new HttpRequest.LiverpoolDataReceiver() {
            @Override
            public void onReceive(ArrayList<Product> productsList) {
                mainActivityViewHandlers.dataFetched(productsList);
            }
            @Override
            public void onErrorReceiving() {
                mainActivityViewHandlers.errorReceiving("error recibiendo data");
            }

            @Override
            public void onNetworkError() {
                mainActivityViewHandlers.errorReceiving("error de red");
            }
        });
        mainActivityViewHandlers =  new MainActivityViewHandlers(this);
        mainActivityViewHandlers.setDataFetcherListener(new MainActivityViewHandlers.DataFetcherListener() {
            @Override
            public void onScrolledDataFetched() {
                if (!requesting) {
                    requesting = true;
                    scrolled = true;
                    lastPageChecked++;
                    httpRequestController.searchItem(lastQuery, lastPageChecked,
                            mainActivity.getApplicationContext());
                }
            }

            @Override
            public void onUnScrolledDataFetched(ArrayList<Product> products) {
                rvAdapter.getProducts().addAll(products);
                rvAdapter.notifyDataSetChanged();
            }
        });
    }

    public boolean requestProductsFromSearcher(String query){
        mainActivityViewHandlers.requestProductsFromSearcher();
        saveQueryInDB(query);
        lastPageChecked = 1;
        lastQuery = query;
        scrolled = false;
        httpRequestController.searchItem(query, lastPageChecked, mainActivity.getApplicationContext());
        return false;
    }

    private void saveQueryInDB(String query){
        //check if query don't exists
        List<String> productsStored = dbHandler.getQueries();
        if (productsStored != null){
            for (String queryStored:productsStored
            ) {
                if (queryStored.equals(query)) {
                    return;
                }
            }
            dbHandler.insertQuery(query);
            suggestionsAddapter = new ArrayAdapter<>(mainActivity, android.R.layout.simple_list_item_1, dbHandler.getQueries());
        }
    }

    public boolean onQuerytexChanged(String query){
        if (query.length() >= 1) {
            mainActivity.getLvSuggestions().setVisibility(View.VISIBLE);
            suggestionsAddapter.getFilter().filter(query);
        }else {
            mainActivity.getLvSuggestions().setVisibility(View.GONE);
        }
        return true;
    }
}
