package com.darien.myapplication.ViewModels.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;

public class PrevSearchesDBHandler extends SQLiteOpenHelper {
    private static final String DB_NAME = "LiverpoolDB.sql";
    private static final int DATABASE_VERSION = 1;

    static abstract class PrevQuerys implements BaseColumns {
        static final String TABLE_NAME = "prev_querys";
        static final String QUERY="licencia"; //string
    }

    public PrevSearchesDBHandler(Context ctx) {
        super(ctx, DB_NAME, null, DATABASE_VERSION);
    }

    //crear la tabla
    private static final String SQL_CREATE_QUERIES_TABLE =
            "CREATE TABLE " + PrevQuerys.TABLE_NAME + " (" +
                    PrevQuerys._ID + " INTEGER PRIMARY KEY," +
                    PrevQuerys.QUERY + " TEXT" +
                    " )";

    //vaciar la tabla
    private static final String SQL_DELETE_QUERIES =
            "DROP TABLE IF EXISTS " + PrevQuerys.TABLE_NAME;

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_QUERIES_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void insertQuery(String query) {
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(PrevQuerys.QUERY, query);
        db.insert(PrevQuerys.TABLE_NAME, null, values);
    }

    public ArrayList<String> getQueries() {
        ArrayList<String> queries = new ArrayList<>();

        String[] projection = {
                PrevQuerys.QUERY,
        };
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(
                PrevQuerys.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        c.moveToFirst();
        for (int i = 0; i < c.getCount(); i++) {
            queries.add(c.getString(0));
            c.moveToNext();
        }
        c.close();
        return queries;
    }

    void deleteQueries(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(PrevQuerys.TABLE_NAME,null,null);
    }
}
