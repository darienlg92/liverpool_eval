package com.darien.myapplication.ViewModels.Controllers;

import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darien.myapplication.Models.Product;
import com.darien.myapplication.ViewModels.MainActivityViewModel;

import java.util.ArrayList;

//clase para manejar las vistas de MainActivity
public class MainActivityViewHandlers {
    private MainActivityViewModel mainActivityViewModel;
    private DataFetcherListener dataFetcherListener;

    public void setDataFetcherListener(DataFetcherListener dataFetcherListener) {this.dataFetcherListener = dataFetcherListener;}

    public MainActivityViewHandlers(MainActivityViewModel mainActivityViewModel) {
        this.mainActivityViewModel = mainActivityViewModel;
    }

    public void errorReceiving(String message){
        mainActivityViewModel.setRequesting(false);
        mainActivityViewModel.getMainActivity().getPbSearching().clearAnimation();
        mainActivityViewModel.getMainActivity().getPbSearching().setVisibility(View.GONE);
        mainActivityViewModel.getMainActivity().getTvIntroduzcaTermino().setVisibility(View.VISIBLE);
        Toast.makeText(mainActivityViewModel.getMainActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
    public void dataFetched(ArrayList<Product> products){
        mainActivityViewModel.setRequesting(false);
        if (!mainActivityViewModel.isScrolled()) {
            mainActivityViewModel.getMainActivity().getPbSearching().clearAnimation();
            mainActivityViewModel.getMainActivity().getPbSearching().setVisibility(View.GONE);
            mainActivityViewModel.getMainActivity().getRvResults().setVisibility(View.VISIBLE);
            mainActivityViewModel.setRvAdapter(new RecyclerViewAdapter(products, mainActivityViewModel
                    .getMainActivity().getApplicationContext()));
            mainActivityViewModel.getMainActivity().getRvResults().setAdapter(mainActivityViewModel.getRvAdapter());
            mainActivityViewModel.getMainActivity().getRvResults().setLayoutManager(new LinearLayoutManager(mainActivityViewModel
                    .getMainActivity().getApplicationContext()));
            mainActivityViewModel.getMainActivity().getRvResults().addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                        if (dataFetcherListener != null){
                            dataFetcherListener.onScrolledDataFetched();
                        }
                    }
                }
            });
        }else {
            //pasar esto a un delegado y manejarlo desde el ViewModel
            if (dataFetcherListener != null){
                dataFetcherListener.onUnScrolledDataFetched(products);
            }
        }
    }

    public void requestProductsFromSearcher(){
        mainActivityViewModel.getMainActivity().getLvSuggestions().setVisibility(View.GONE);
        mainActivityViewModel.getMainActivity().getRvResults().setVisibility(View.GONE);
        mainActivityViewModel.getMainActivity().getTvIntroduzcaTermino().setVisibility(View.GONE);
        mainActivityViewModel.getMainActivity().getPbSearching().setVisibility(View.VISIBLE);
        mainActivityViewModel.getMainActivity().getPbSearching().animate();
    }

    public interface DataFetcherListener{
        void onScrolledDataFetched();
        void onUnScrolledDataFetched(ArrayList<Product> products);
    }
}
