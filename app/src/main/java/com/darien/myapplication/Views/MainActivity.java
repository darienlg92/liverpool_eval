package com.darien.myapplication.Views;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import com.darien.myapplication.ViewModels.MainActivityViewModel;
import com.darien.myapplication.R;

public class MainActivity extends AppCompatActivity {
    private ProgressBar pbSearching;
    private RecyclerView rvResults;
    private TextView tvIntroduzcaTermino;
    private MainActivityViewModel mainActivityViewModel;
    private ListView lvSuggestions;
    private SearchView searchView;

    public ProgressBar getPbSearching() {return pbSearching;}
    public RecyclerView getRvResults() {return rvResults;}
    public TextView getTvIntroduzcaTermino() {return tvIntroduzcaTermino;}
    public ListView getLvSuggestions() {return lvSuggestions;}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initVariables();
    }

    private void initVariables(){
        pbSearching = findViewById(R.id.pb_searching);
        rvResults = findViewById(R.id.rv_results);
        tvIntroduzcaTermino = findViewById(R.id.tv_introduzca_termino);
        lvSuggestions = findViewById(R.id.lv_suggestions);
        mainActivityViewModel = new MainActivityViewModel(this);
        lvSuggestions.setAdapter(mainActivityViewModel.getSuggestionsAddapter());
        lvSuggestions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lvSuggestions.setVisibility(View.GONE);
                TextView tvText = view.findViewById(android.R.id.text1);
                searchView.setQuery(tvText.getText().toString(), true);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Buscar...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return mainActivityViewModel.requestProductsFromSearcher(s);
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return mainActivityViewModel.onQuerytexChanged(s);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
